import React from 'react';
import Link from 'next/link';
import Head from 'next/head';
import Layout from './../components/Layout';

const Kontakt = () => (
  <Layout>
    <Head>EnergoRent</Head>
    <section className="page-section" id="contact">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center">
            <div className="card bg-dark text-white">
              <img
                src="/static/img/map-image.png"
                className="card-img"
                alt="Kontakt"
              />
              <div className="card-img-overlay">
                <h1 className="card-title">Kontakt</h1>
                <p className="card-text">
                  EnergoRent
                  <br />
                  Cara Dušana 70
                  <br />
                  21000 Novi Sad
                  <br />
                  Srbija
                  <br />
                </p>
                <p className="card-text display-4">
                  <i className="fas fa-at"></i> energorent@gmail.com
                  <br />
                  <i className="fas fa-phone"></i> +381 69 555-555
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </Layout>
);

export default Kontakt;
