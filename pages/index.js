import React from 'react';
import Link from 'next/link';
import Head from 'next/head';
import Layout from './../components/Layout';

const Home = () => (
  <Layout>
    <Head>EnergoRent</Head>
    <section className="page-section" id="services">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center">
            <h2 className="section-heading text-uppercase">EnergoRent</h2>
            <h3 className="section-subheading text-muted">
              Vaš pouzdan partner za rezervno napajanje.
            </h3>
          </div>
        </div>
        <div className="row text-center">
          <div className="col-md-4">
            <span className="fa-stack fa-4x">
              <i className="fas fa-circle fa-stack-2x text-primary"></i>
              <i className="fas fa-shopping-cart fa-stack-1x fa-inverse"></i>
            </span>
            <h4 className="service-heading">Prodaja</h4>
            <p className="text-muted">
              Ukoliko želite da kupite benzinski ili dizel elektro agregatat, mi
              vam možemo pomoći da odaberete pravi agregat za vas. U ponudi
              imamo veliki raspon snaga i konfiguracija, za sve vaše potrebe.
            </p>
          </div>
          <div className="col-md-4">
            <span className="fa-stack fa-4x">
              <i className="fas fa-circle fa-stack-2x text-primary"></i>
              <i className="fas fa-charging-station fa-stack-1x fa-inverse"></i>
            </span>
            <h4 className="service-heading">Rentiranje</h4>
            <p className="text-muted">
              Potreban vam je agregat na jedan dan, dve nedelje ili duži
              vremenski period. Mi vam stojimo na usluzi sa našom flotom
              agregata, uvek spremih da ih iznajmite / rentirate.
            </p>
          </div>
          <div className="col-md-4">
            <span className="fa-stack fa-4x">
              <i className="fas fa-circle fa-stack-2x text-primary"></i>
              <i className="fas fa-tools fa-stack-1x fa-inverse"></i>
            </span>
            <h4 className="service-heading">Servis</h4>
            <p className="text-muted">
              Redovno i korektivno održavanje dizel i benzinskih agregata. Naš
              stručni tim vam je na usluzi za kako za popravke, tako i za
              popravke vaših elektro agregata.
            </p>
          </div>
        </div>
      </div>
    </section>
  </Layout>
);

export default Home;
