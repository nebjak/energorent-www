import React from 'react';
import Link from 'next/link';

const Nav = () => (
  <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div className="container">
      <a className="navbar-brand js-scroll-trigger" href="/">
        <img src="/static/img/logo.png" height="30" alt="EnergoRent" />
      </a>
      <button
        className="navbar-toggler navbar-toggler-right"
        type="button"
        data-toggle="collapse"
        data-target="#navbarResponsive"
        aria-controls="navbarResponsive"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        Menu
        <i className="fas fa-bars"></i>
      </button>
      <div className="collapse navbar-collapse" id="navbarResponsive">
        <ul className="navbar-nav text-uppercase ml-auto">
          <li className="nav-item">
            <Link href="/">
              <a className="nav-link" href="/">
                O nama
              </a>
            </Link>
          </li>
          <li className="nav-item">
            <Link href="/kontakt">
              <a className="nav-link" href="/">
                Kontakt
              </a>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  </nav>
);

export default Nav;
