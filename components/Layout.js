import React from 'react';
import Link from 'next/link';
import Head from 'next/head';
import Nav from './Nav';
import Header from './Header';
import Footer from './Footer';

const Layout = props => (
  <div>
    <Head>
      <link rel="stylesheet" href="/static/css/bootstrap.min.css" />
      <link
        href="/static/fontawesome-free/css/all.min.css"
        rel="stylesheet"
        type="text/css"
      />
      <link
        href="https://fonts.googleapis.com/css?family=Montserrat:400,700"
        rel="stylesheet"
        type="text/css"
      />
      <link
        href="https://fonts.googleapis.com/css?family=Kaushan+Script"
        rel="stylesheet"
        type="text/css"
      />
      <link
        href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic"
        rel="stylesheet"
        type="text/css"
      />
      <link
        href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700"
        rel="stylesheet"
        type="text/css"
      />
      <link rel="stylesheet" href="/static/css/agency.css" />
    </Head>
    <Nav />
    <Header />
    <div>{props.children}</div>
    <Footer />
  </div>
);

export default Layout;
