import React from 'react';

const Header = () => (
  <header className="masthead">
    <div className="container">
      <div className="intro-text">
        <div className="intro-lead-in">&nbsp;</div>
        <div className="intro-heading text-uppercase">&nbsp;</div>
        <a
          className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
          href="#services"
        >
          Više o nama
        </a>
      </div>
    </div>
  </header>
);

export default Header;
